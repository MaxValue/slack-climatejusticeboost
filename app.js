const { App } = require("@slack/bolt");
const Crypto = require("crypto");
var models  = require("./models");
const { Op } = require("sequelize");

const app = new App({
  signingSecret: process.env.SLACK_SIGNING_SECRET,
  clientId: process.env.SLACK_CLIENT_ID,
  clientSecret: process.env.SLACK_CLIENT_SECRET,
  stateSecret: Crypto.randomBytes(40).toString("base64").slice(0, 40),
  scopes: [
  //   "channels:manage", // To add users to open channels.
  //   "channels:read", // To know which public channels the bot has access to.
  //   "commands", // To support context menu items for this bot.
  //   "groups:read", // To know which private channels the bot has access to.
  //   "groups:write", // To add users to private channels.
  //   "im:read", // To know which private chats (one person with this bot) the bot has access to.
  //   "mpim:read", // To know which group chats (multiple persons with this bot) the bot has access to.
  //   "reactions:read", // For polls
  //   "team:read", // Support base functionality
  //   "usergroups:read", // Automatically manage user groups and optionally synchronize them with an external system.
    "commands", // To support context menu items for this bot.
    "chat:write", // To post messages.
    "chat:write.customize" // To personalize messages sent by the bot user.
  ],
  installerOptions: {
    userScopes: [
      "chat:write", // To post messages containing the invite links.
      "channels:write", // To add users to open channels and to focus the app on an open channel.
      "groups:write", // To add users to private channels and to focus the app on a private channel.
      "channels:read", // To get the name of the public channel you want to invite users into.
      "groups:read", // To get the name of the private channel you want to invite users into.
    ],
  },
  installationStore: {
    storeInstallation: async (installation) => {
      models.App.upsert({
        slackId: installation.appId
      }).then(resultApp => {
        models.Workspace.upsert({
          app_id: resultApp[0].dataValues.id,
          slackId: installation.team.id,
          name: installation.team.name,
        },{
          fields: [
            "slackId",
            "name",
          ]
        }).then(resultWorkspace => {
          Promise.all([
            models.User.upsert({
              slackId: installation.user.id,
              // scopes: installation.user.scopes.join(),
              token: installation.user.token,
              workspace_id: resultWorkspace[0].dataValues.id
            }),
            models.Bot.upsert({
              slackId: installation.bot.id,
              userId: installation.bot.userId,
              // scopes: installation.bot.scopes.join(),
              token: installation.bot.token,
              workspace_id: resultWorkspace[0].dataValues.id
            }),
          ]).then(resultUserBot => {
            return true;
          }).catch(error => {
            return false
          });
        })
      })
    },
    fetchInstallation: async (InstallQuery) => {
      return await models.Workspace.findOne({
        where: {
          slackId: InstallQuery.teamId
        },
        include: [
          {
            association: "app"
          },
          {
            association: "users",
            where: {
              slackId: InstallQuery.userId
            }
          },
          {
            association: "bots"
          },
        ]
      }).then(result => {
        const returnObject = {
          appId: result.app.slackId,
          user: {
            id: result.users[0].slackId,
            // scopes: result.users[0].scopes.split(","),
            token: result.users[0].token,
          },
          bot: {
            id: result.bots[0].slackId,
            // userId: result.bots[0].userId,
            // scopes: result.bots[0].scopes.split(","),
            token: result.bots[0].token,
          },
        };
        return returnObject;
      })
    }
  }
});

const home = require("./features/home");
home.init(app);
require("./features/invitelink")(app);

(async () => {
  await app.start(process.env.PORT || 3000);

  console.log("🌟 Climate Justice Boost is running!");
})();

