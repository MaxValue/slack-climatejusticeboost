module.exports = {
  inferConversationType: function(channelInfo) {
    if (!channelInfo.is_group && channelInfo.is_channel && !channelInfo.is_im && !channelInfo.is_mpim) {
      return "open"
    }
    else if (channelInfo.is_group && !channelInfo.is_channel && !channelInfo.is_im && !channelInfo.is_mpim) {
      return "closed"
    }
    else if (!channelInfo.is_group && !channelInfo.is_channel && !channelInfo.is_im && channelInfo.is_mpim) {
      return "mpim"
    }
    else if (!channelInfo.is_group && !channelInfo.is_channel && channelInfo.is_im && !channelInfo.is_mpim) {
      return "im"
    }
  },
  isError: function isError(errordata, errorcode, suberrorcode) {
    if (!errordata.ok && errordata.error == errorcode) {
      if (suberrorcode) {
        return errordata.errors.find(slackError => slackError.error == suberrorcode);
      } else {
        return true;
      }
    } else {
      return false;
    }
  }
}
