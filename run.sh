#!/usr/bin/env bash
set -o allexport
source $1.env
set +o allexport
source ~/.asdf/asdf.sh
NODE_ENV=$1 node app.js
