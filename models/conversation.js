'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Conversation extends Model {
    static associate(models) {
      Conversation.belongsTo(models.Workspace, {
        foreignKey: "workspace_id",
        as: "workspace",
        allowNull: false
      });
      Conversation.belongsToMany(models.User, {
        through: "ConversationMembers",
        as: "members",
        foreignKey: "conversation_id"
      });
      Conversation.hasMany(models.Invitelink, {
        as: "invitelinks",
        foreignKey: "conversation_id"
      });
    }
  };
  Conversation.init({
    slackId: {
      type: DataTypes.STRING,
      unique: true,
    },
    name: DataTypes.STRING,
    type: DataTypes.ENUM("open","closed","mpim","im"),
    archived: DataTypes.BOOLEAN
  }, {
    sequelize,
    modelName: 'Conversation',
  });
  return Conversation;
};
