
# Installation
1. Copy /host_vars/hostname.yml.tpl to /host_vars/hostname.yml where `hostname` is replaced with the name of your server. This can be done multiple times for as many hosts a you like. Don't forget to fill in the details for your setup.
2. Copy /development.yml.tpl to /host_vars/development.yml and fill in `DEVHOSTNAME` with the hostname where you are deploying your development setup to.
3. Copy /test.yml.tpl to /host_vars/test.yml and fill in `TESTHOSTNAME` with the hostname where you are deploying your test setup to.
4. Copy /production.yml.tpl to /host_vars/production.yml and fill in `PRODUCTIONHOSTNAME` with the hostname where you are deploying your production setup to.
5. Deploy:
  * Development:
  ```bash
  ansible-playbook -i development.yml -e "PWD=$PWD" deploy.yml
  ```
  * Test:
  ```bash
  ansible-playbook -i test.yml -e "PWD=$PWD" deploy.yml
  ```
  * Production:
  ```bash
  ansible-playbook -i production.yml -e "PWD=$PWD" deploy.yml
  ```
  You get the drift. Unfortunately, i think you cannot add more stages, because the database only supports those 3. But I never tried, so maybe it just parses the environments you defined in `/config/config.json`.

# Database
## Schema
Apps
  Workspaces
    Conversations
    Users
    Bots
    Invitelinks
    UserConversation

## Changing the database
1. (create model)
2. (create migration)
3. (add relations to models)
4. (add foreign keys to migration file)
5. (add connection tables to migration file)
6. replace ' with "
7. test
8. commit
9. deploy
