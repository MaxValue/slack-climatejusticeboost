var models  = require("../models");
const { inferConversationType, isError } = require("../util");
const { Op } = require("sequelize");
const home = require("./home");

module.exports = function(app) {

  // when context menu item is clicked
  app.shortcut('post_invitelink', async ({ shortcut, ack, client }) => {

    try {
      await ack();

      const result = await client.views.open({
        trigger_id: shortcut.trigger_id,
        view: {
          type: "modal",
          callback_id: "submit_invitelink",
          private_metadata: `${shortcut.channel.id} ${shortcut.message.thread_ts||shortcut.message_ts}`,
          title: {
            type: "plain_text",
            text: "Einladungslink"
          },
          close: {
            type: "plain_text",
            text: "Abbrechen"
          },
          submit: {
            type: "plain_text",
            text: "Erstellen"
          },
          clear_on_close: true,
          blocks: [
            {
              "type": "input",
              "block_id": "block_chosenchannel",
              "element": {
                "type": "conversations_select",
                "placeholder": {
                  "type": "plain_text",
                  "text": "Channel auswählen",
                  "emoji": true
                },
                "filter": {
                  "include": [
                    "private",
                    "public"
                  ],
                  "exclude_external_shared_channels": true,
                  "exclude_bot_users": true
                },
                "action_id": "action_chosenchannel"
              },
              "label": {
                "type": "plain_text",
                "text": "In welchen Channel soll eingeladen werden?",
                "emoji": true
              },
              "hint": {
                "type": "plain_text",
                "text": "Du kannst auch einen öffentlichen Channel auswählen, um auf jenen Channel aufmerksam zu machen."
              }
            }
          ]
        }
      });
    }
    catch (error) {
      console.error(error);
    }
  });

  // when modal (see above) is submitted
  app.view('submit_invitelink', async ({ ack, body, view, client }) => {
    await ack();

    const channelId = view['state']['values']['block_chosenchannel']['action_chosenchannel']['selected_conversation'];
    const message = view.private_metadata.split(" ");
    const channelAnnounce = message[0];
    const messageAnnounce = message[1];

    try {

      dbWorkspace = await models.Workspace.findOne({
        where: {
          slackId: body.team.id
        },
        include: [
          {
            association: "users",
            where: {
              slackId: body.user.id
            }
          }
        ]
      });
      conversationInfo = await client.conversations.info({
        token: dbWorkspace.users[0].token,
        channel: channelId
      });
      conversation = (await models.Conversation.upsert({
        workspace_id: dbWorkspace.id,
        slackId: channelId,
        name: conversationInfo.channel.name,
        type: inferConversationType(conversationInfo.channel),
        archived: conversationInfo.channel.is_archived
      }, {
        fields: [
          "name",
          "type",
          "archived",
        ]
      }))[0];
      const newInvitelink = await models.Invitelink.create({
        conversation_id: conversation.id,
        user_id: dbWorkspace.users[0].id
      });
      await client.chat.postMessage({
        token: dbWorkspace.users[0].token,
        channel: channelAnnounce,
        thread_ts: messageAnnounce,
        text: `Öffne die Nachricht um dem Channel #${conversation.name} beitreten zu können.`,
        //          if same user replied last in thread: change message (append button)
        //          elif other user: in thread
        blocks: [
          {
            "type": "section",
            "text": {
              "type": "mrkdwn",
              "text": `Hier klicken um dem Channel <slack://channel?team=${dbWorkspace.slackId}&id=${conversation.slackId}|#${conversation.name}> beizutreten:`,
            }
          },
          {
            "type": "actions",
            "elements": [
              {
                "type": "button",
                "text": {
                  "type": "plain_text",
                  "text": "Beitreten",
                  "emoji": true
                },
                "style": "primary",
                "value": `${newInvitelink.id}`,
                "action_id": "button_join"
              }
            ]
          }
        ]
      });
      await home.render(client, body.api_app_id, body.team.id, body.user.id);
    }
    catch (error) {
      console.error(error);
    }

  });

  // when the "Beitreten" button is clicked in the message (posted by the method above)
  app.action('button_join', async ({ ack, client, body }) => {
    await ack();

    // get data: message, channel

    const callingUser = body.user;
    const sourceMessage = body.message;
    var doInvite = true;
    models.App.findOne({
      where: {
        slackId: body.api_app_id
      },
      include: [{
        association: "workspaces",
        where: {
          slackId: body.team.id
        },
        include: [{
          association: "users",
          where: {
            slackId: body.message.user
          },
          include: [{
            association: "invitelinks",
            where: {
              id: body.actions[0].value
            },
            include: [
              {
                association: "conversation"
              },
              {
                association: "owner"
              }
            ]
          }]
        }]
      }]
    })
    .then(async (dbApp) => {
      const dbInvitelink = dbApp.workspaces[0].users[0].invitelinks[0];
      if (dbInvitelink.active) {
        try {
          const result = await client.conversations.invite({
            token: dbInvitelink.owner.token,
            channel: dbInvitelink.conversation.slackId,
            users: callingUser.id
          });
        } catch (error) {
          if (error.code == "slack_webapi_platform_error") {
            if (isError(error.data, "cant_invite", "cant_invite_self")) {
              await client.chat.postEphemeral({
                icon_emoji: ":repeat:",
                channel: body.channel.id,
                thread_ts: sourceMessage.thread_ts,
                user: callingUser.id,
                text: "Du kannst dich nicht selbst einladen!"
              })
            } else if (isError(error.data, "channel_not_found")) {
              await client.chat.postEphemeral({
                icon_emoji: ":shrug:",
                channel: body.channel.id,
                thread_ts: sourceMessage.thread_ts,
                user: callingUser.id,
                text: `<@${dbInvitelink.owner.slackId}> ist nicht mehr im Channel und kann dich daher nicht einladen.`
              })
            } else if (isError(error.data, "is_archived")) {
              await client.chat.postEphemeral({
                icon_emoji: ":file_cabinet:",
                channel: body.channel.id,
                thread_ts: sourceMessage.thread_ts,
                user: callingUser.id,
                text: `Damn. Der Channel ist bereits archiviert, daher kann niemand mehr beitreten.`
              })
            } else {
              throw error;
            }
          }
        }
      } else {
        await client.chat.postEphemeral({
          icon_emoji: ":no_entry:",
          channel: body.channel.id,
          thread_ts: sourceMessage.thread_ts,
          user: callingUser.id,
          text: "Dieser Einladungslink ist nicht mehr gültig!"
        })
      }
    }).catch(async (error) => {
      console.error(error);
      await client.chat.postEphemeral({
        icon_emoji: ":interrobang:",
        channel: body.channel.id,
        thread_ts: sourceMessage.thread_ts||body.message_ts,
        user: callingUser.id,
        text: "FEHLER Einladung fehlgeschlagen"
      })
    });
  });

  app.action('toggle_invitelink', async ({ ack, client, body, action }) => {
    await ack();
    const invitelinkRange = action.block_id.split(" ");
    models.App.findOne({
      where: {
        slackId: body.api_app_id
      },
      include: [{
        association: "workspaces",
        where: {
          slackId: body.team.id
        },
        include: [{
          association: "users",
          where: {
            slackId: body.user.id
          },
          include: [{
            association: "invitelinks",
            include: [{
              association: "conversation"
            }],
            where: {
              id: {
                [Op.gte]: invitelinkRange[0],
                [Op.lte]: invitelinkRange[1],
              }
            },
            order: [['id', 'ASC']]
          }]
        }]
      }]
    })
    .then(async (dbApp) => {
      let needsRerender = false;
      dbApp.workspaces[0].users[0].invitelinks.forEach(invitelink => {
        if (!action.selected_options.find(checkbox => parseInt(checkbox.value) == invitelink.id)) {
          needsRerender = true;
          invitelink.destroy();
        }
      });
      if (needsRerender) {
        await home.render(client, body.api_app_id, body.team.id, body.user.id);
      }
    }).catch(error => console.error(error));
  });
}
